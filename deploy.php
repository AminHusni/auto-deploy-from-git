<?php
	// The commands
	$commands = array(
		'echo $PWD',
		'whoami',
		'git stash',
		'git pull',
		'git status',
		'git submodule sync',
		'git submodule update',
		'git submodule status',
		
	);
	// Run the commands for output
	$output = '';
    chdir("/var/www/laravel/"); //Your root of git directory(incase that the php must be called from somewhere else such in Laravel's case')
	foreach($commands AS $command){
		// Run it
		$tmp = shell_exec($command);
		// Output
		$output .= "<span style=\"color: #6BE234;\">\$</span> <span style=\"color: #729FCF;\">{$command}\n</span>";
		$output .= htmlentities(trim($tmp)) . "\n";
	}
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>GIT DEPLOYMENT SCRIPT[AMIN HUSNI]</title>
</head>
<body style="background-color: #d3d3d3; color: #FFFFFF; font-weight: bold; padding: 0 10px;">
<pre>
| Git Deployment Script


<?php echo $output; ?>
</pre>
</body>
</html>